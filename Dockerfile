FROM ubuntu:18.04

COPY scripts/installcert.sh /tmp

RUN apt-get update -y && \
    apt-get upgrade -y && \
    apt-get install -y \
      git \
      openssh-client && \
    /tmp/installcert.sh
