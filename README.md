# ssh-git

A basic container with ssh/git installed. Mostly useful for syncing from gitlab to github via gitlab-ci jobs.

Example gitlab-ci job:
```
sync:
  stage: sync
  image: gitlab.corp.evernym.com:4567/dev/containers/ssh-git:latest
  tags:
    - docker
  only:
    refs:
      - master@group/my-awesome-project
  script:
    - mkdir -p ~/.ssh
    - ssh-keyscan -4H github.com > ~/.ssh/known_hosts
    - eval $(ssh-agent -s)
    - echo "$GITHUB_DEPLOY_KEY" | tr -d '\r' | ssh-add - > /dev/null
    - git checkout master && git pull origin master
    - git remote add github git@github.com:github-group/my-awesome-project.git
    - git push github
```

You will need to create a deploy key for this. The public half would be attached to the project on github with write access enabled, and the private half would be put in an environment variable named `GITHUB_DEPLOY_KEY` through the gitlab ui
